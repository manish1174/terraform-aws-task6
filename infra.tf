provider "kubernetes" {
  config_context_cluster   = "minikube"
}

provider "aws" {
  region = "ap-south-1"
  profile    = "manish"
}

resource "kubernetes_deployment" "wordpress" {
  metadata {
    name = "wordpress"
    labels = {
      app = "wordpress"
    }
  }
  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "wordpress"
      }
    }

    template {
      metadata {
        labels = {
          app = "wordpress"
        }
      }

      spec {
        container {
          image = "wordpress:4.8-apache"
          name  = "wordpress"
        }
    }
  }
}
}

resource "kubernetes_service" "wordpress-service" {
  
  depends_on = [
    kubernetes_deployment.wordpress,
  ]
  
  metadata {
    name = "wordpress-service"
  }
  spec {
    selector = {
      app = "${kubernetes_deployment.wordpress.metadata.0.labels.app}"
    }

    port {
      port        = 80
      target_port = 80
    }

    type = "NodePort"
  }
}


resource "aws_db_instance" "databaseRDS" {
  allocated_storage          = 8
  max_allocated_storage      = 10
  storage_type               = "gp2"
  engine                     = "mysql"
  engine_version             = "5.7"
  instance_class             = "db.t2.micro"
  identifier                 = "database"
  name                       = "db"
  username                   = "root"
  password                   = "1234manish1234"
  parameter_group_name       = "default.mysql5.7"
  skip_final_snapshot        = true
  port                       = 3306
  publicly_accessible        = true
  auto_minor_version_upgrade = true
  delete_automated_backups   = true
  iam_database_authentication_enabled = true
//  vpc_security_group_ids = ["sg-0ae001d1197310879"]
}
output "databaseIP" {
  value = aws_db_instance.databaseRDS.address
}
